import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Laleli Collection';
  openNav=false;
  openRightNav(){
    this.openNav=true;
  }
  closeRightNav(){
    this.openNav=false;
  }
}
